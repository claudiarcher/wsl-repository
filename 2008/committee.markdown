## Program Committee

* Adenauer Yamin, Universidade Federal de Pelotas/Universidade Católica de Pelotas, Brazil
* Alexandre Ribeiro, Universidade de Caxias do Sul, Brazil
* Alfredo Goldman, Universidade de São Paulo, Brazil
* André Detsch, IBM, Brazil
* Andrea Charão, Universidade Federal de Santa Maria, Brazil
* Carla Alessandra Lima Reis, Universidade Federal do Pará, Brazil
* Carlos de Salles Soares Neto, Pontifícia Universidade Católica do Rio de Janeiro, Brazil
* Celso Maciel da Costa, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Cristiana Bentes, Universidade do Estado do Rio de Janeiro, Brazil
* Cristiano André da Costa, Universidade do Vale do Rio dos Sinos, Brazil
* Ewerton Longoni Madruga, Instituto Nacional de Metrologia, Qualidade e Tecnologia, Brazil
* Fernando Osório, Universidade do Vale do Rio dos Sinos, Brazil
* Gaspare Bruno, Centro Universitário La Salle, Brazil
* Gerson Cavalheiro, Universidade Federal de Pelotas, Brazil
* Hisham Hashem Muhammad, Gobolinux.org, Brazil
* João Cesar Netto, Universidade Federal do Rio Grande do Sul, Brazil
* Jussara Musse, Universidade Federal do Rio Grande do Sul, Brazil
* Leonardo Lemes Fagundes, Universidade do Vale do Rio dos Sinos, Brazil
* Lisandro Zambenedetti Granville, Universidade Federal do Rio Grande do Sul, Brazil
* Luciano Paschoal Gaspary, Universidade Federal do Rio Grande do Sul, Brazil
* Luciano Porto Barreto, Universidade Federal da Bahia, Brazil
* Márcia Pasin, Universidade Federal de Santa Maria, Brazil
* Marco Mangan, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Marcos Castilho, Universidade Federal do Paraná, Brazil
* Marilton Aguiar, Universidade Católica de Pelotas, Brazil
* Marinho Pilla Barcellos, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Mario Domenech Goulart, Universidade Católica de Pelotas, Brazil
* Marluce Rodrigues Pereira, Universidade Federal de Lavras, Brazil
* Mauricio Lima Pilla, Universidade Católica de Pelotas, Brazil
* Patrícia Kayser Vargas Mangan, Centro Universitário La Salle, Brazil
* Paulyne Matthews Jucá, Centro de Estudos e Sistemas Avançados do Recife, Brazil
* Rafael dos Santos, Universidade de Santa Cruz do Sul, Brazil
* Regina Borges de Araujo, Universidade Federal de São Carlos, Brazil
* Rejane Frozza, Universidade de Santa Cruz do Sul, Brazil
* Renata Galante, Universidade Federal do Rio Grande do Sul, Brazil
* Rodrigo Araújo Real, Universidade Católica de Pelotas, Brazil
* Rodrigo Quites Reis, Universidade Federal do Pará, Brazil
* Sílvio César Cazella, Universidade do Vale do Rio dos Sinos, Brazil

## Organizing Committee

General Chair

* Ewerton Longoni Madruga (Instituto Nacional de Metrologia, Qualidade e Tecnologia, Brazil)

Program Committee Chair

* Patrícia Kayser Vargas Mangan (Centro Universitário La Salle, Brazil)
