## Program Committee

* Alexandre Moretto Ribeiro, Universidade de Caxias do Sul, Brazil
* Cláudio Fernando Resin Geyer, Universidade Federal do Rio Grande do Sul, Brazil
* Daniel Nehme Muller, Universidade Luterana do Brasil, Brazil
* Daniela Leal Musa, Universidade Federal do Rio Grande do Sul, Brazil
* Denise Bandeira da Silva, Universidade do Vale do Rio dos Sinos, Brazil
* Edgar Meneghetti, Universidade de Caxias do Sul, Brazil
* Elgio Schlemer, Universidade Luterana do Brasil, Brazil
* Fabio Zschornack, Universidade Federal do Rio Grande do Sul, Brazil
* Fernando Santos Osório, Universidade do Vale do Rio dos Sinos, Brazil
* Francisco Assis Moreira do Nascimento, Universidade Luterana do Brasil, Brazil
* Gerson Geraldo Homirch Cavalheiro, Universidade do Vale do Rio dos Sinos, Brazil
* João Batista Oliveira, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* João Cesar Netto, Universidade Federal do Rio Grande do Sul, Brazil
* José Palazzo Moreira de Oliveira, Universidade Federal do Rio Grande do Sul, Brazil
* Leandro Krug Wives, Universidade Feevale, Brazil
* Lisandro Zambenedetti Granville, Universidade Federal do Rio Grande do Sul, Brazil
* Luciano Paschoal Gaspary, Universidade do Vale do Rio dos Sinos, Brazil
* Luciana Porcher Nedel, Universidade Federal do Rio Grande do Sul, Brazil
* Manuel Menezes de Oliveira Neto, Universidade Federal do Rio Grande do Sul, Brazil
* Maria Janilce Bosquiroli Almeida, Universidade Federal do Rio Grande do Sul, Brazil
* Marinho Pilla Barcelos, Universidade do Vale do Rio dos Sinos, Brazil
* Mariusa Warpechowski, Universidade Federal do Rio Grande do Sul, Brazil
* Paulo Roberto Ferreira Jr., Universidade Feevale, Brazil
* Renata Zanella, Universidade Federal do Rio Grande do Sul, Brazil
* Roland Teodorowitsch, Universidade Luterana do Brasil, Brazil
* Simone André da Costa, Universidade do Vale do Rio dos Sinos, Brazil
* Taisy Weber, Universidade Federal do Rio Grande do Sul, Brazil

## Organizing Committee

General Chair

* Denise Bandeira da Silva (Universidade do Vale do Rio dos Sinos, Brazil)

Program Committee Chair

* Lisandro Zambenedetti Granville (Universidade Federal do Rio Grande do Sul, Brazil)

Organizing Committee

* Alexandre Moretto Ribeiro (Universidade de Caxias do Sul, Brazil)
* César Augusto Fonticielha de Rose (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)
* Luciana Porcher Nedel (Universidade Federal do Rio Grande do Sul, Brazil)
* Roland Teodorowitsch (Universidade Luterana do Brasil, Brazil)
