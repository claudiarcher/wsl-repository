## Program Committee

* Aaron Shaw, University of California, Berkeley, United States
* Alexandre Hannud Abdo, Fundação Oswaldo Cruz, Brazil
* Andre Leon S. Gradvohl, Universidade Estadual de Campinas, Brazil
* Andrea Schwertner Charão, Universidade Federal de Santa Maria, Brazil
* Antonio Terceiro, Universidade Federal da Bahia, Brazil
* Björn Lundell, University of Skövde, Sweden
* Carlos D. Santos Jr, Universidade de Brasília, Brazil
* Carlos de Salles Soares Neto, Universidade Federal do Maranhão, Brazil
* Christina Chavez, Universidade Federal da Bahia, Brazil
* Daniel Batista, Universidade de São Paulo, Brazil
* Fernando Castor, Universidade Federal de Pernambuco, Brazil
* Fernando Osório, Universidade de São Paulo, Brazil
* Gregorio Robles, Universidad Rey Juan Carlos, Spain
* Henrique Parra, Universidade Federal de São Paulo, Brazil
* Israel Herraiz, Universidad Politécnica de Madrid, Spain
* Jesús M. González Barahona, Universidad Rey Juan Carlos, Spain
* José Carlos Maldonado, Universidade de São Paulo, Brazil
* Lisandro Zambenedetti Granville, Universidade Federal do Rio Grande do Sul, Brazil
* Luciana Tricai Cavalini, Universidade Federal Fluminense, Brazil
* Luis Felipe Murillo, University of California, United States
* Marcelo Finger, Universidade de São Paulo, Brazil
* Marcia Pasin, Universidade Federal de Santa Maria, Brazil
* Marcos Castilho, Universidade Federal do Paraná, Brazil
* Marcos Sunye, Universidade Federal do Paraná, Brazil
* Martin Michlmayr, Hewlett-Packard, United Kingdom
* Paulo Meirelles, Universidade de São Paulo, Brazil
* Rafael Evangelista, Universidade Estadual de Campinas, Brazil
* Renata Galante, Universidade Federal do Rio Grande do Sul, Brazil
* Rivalino Matias Jr., Universidade Federal de Uberlândia, Brazil
* Yuri Takhteyev, University of Toronto, Canada

## Organizing Committee

General Chair

* Fabio Kon (Universidade de São Paulo, Brazil)

Program Committee Co-Chairs

* Daniel Batista (Universidade de São Paulo, Brazil)
* Gregorio Robles (Universidad Rey Juan Carlos, Spain)

Local Arrangements Chair

* Carlos Machado (SERPRO, Brazil)

Publicity Chair

* Paulo Meirelles (Universidade de São Paulo, Brazil)

Publications Chair

* Antonio Terceiro (Universidade Federal da Bahia, Brazil)
