require 'fileutils'

papers = %[
  0018 0021
  0022 0025
  0026 0030
  0032 0035
  0036 0039
  0040 0044
  0045 0048
  0049 0052
  0053 0056
  0057 0060
  0061 0064
  0065 0068
  0069 0072
  0073 0076
  0077 0080
  0081 0084
  0085 0088
  0089 0092
  0093 0096
]


def cmd(*args)
  puts args.join(' ')
  system *args
end

cmd 'mkdir', '-p', 'cropped'

papers.lines.map(&:strip).reject(&:empty?).map(&:split).each_with_index do |set, i|
  jpg_files = (set[0]..set[1]).map { |n| "scan/digitalizar#{n}.jpg" }
  cropped_jpg_files = jpg_files.map do |scan|
    cropped = File.join('cropped', File.basename(scan))
    cmd 'convert', scan, '-crop', '877x1334+0+0', cropped
    cropped
  end

  output = "paper%02d.pdf" % (i+1)
  cmd('convert', *cropped_jpg_files, output)
end

cmd 'rm', '-rf', 'cropped'
