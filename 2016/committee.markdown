## Program Committee

* Alfredo Goldman, Universidade de São Paulo, Brazil
* Antonio Terceiro, Linaro/Debian, Brazil
* Aracele Torres, Universidade de São Paulo, Brazil
* Auri Vicenzi, Universidade Federal de São Carlos, Brazil
* Carlos  Denner dos Santos, Universidade de Brasília, Brazil
* Christina Chavez, Universidade Federal da Bahia, Brazil
* Daniel Batista, Universidade de São Paulo, Brazil
* Eduardo Guerra, Instituto Nacional de Pesquisas Espaciais, Brazil
* Fabio Kon, Universidade de São Paulo, Brazil
* Fabricio Solagna, Universidade Federal do Rio Grande do Sul, Brazil
* Francisco José Monaco, Universidade de São Paulo, Brazil
* Gregorio Robles, Universidad Rey Juan Carlos, Spain
* Gregory Madey, University of Notre Dame, United States
* Igor Steinmacher, Universidade Tecnológica Federal do Paraná, Brazil
* Islene Calciolari Garcia, Universidade Estadual de Campinas, Brazil
* Joaquim Uchôa, Universidade Federal de Lavras, Brazil
* José Carlos Maldonado, Universidade de São Paulo, Brazil
* Kevin Crowston, Syracuse University School of Information Studies, United States
* Marcelo Finger, Universidade de São Paulo, Brazil
* Marco Aurélio Graciotto Silva, Universidade Tecnológica Federal do Paraná, Brazil
* Marco Gerosa, Universidade de São Paulo, Brazil
* Nelson Pretto, Universidade Federal da Bahia, Brazil
* Paulo Meirelles, Universidade de Brasília, Brazil
* Rafael Evangelista, Universidade Estadual de Campinas, Brazil
* Rafael Prikladnicki, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Reginaldo Ré, Universidade Tecnológica Federal do Paraná, Brazil
* Roberto Bittencourt, Universidade Estadual de Feira de Santana, Brazil
* Rodrigo Rocha Gomes e Souza, Universidade Federal da Bahia, Brazil
* Sabrina Marczak, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Sandro Andrade, Instituto Federal de Educação, Ciência e Tecnologia da Bahia, Brazil
* Scott Hissam, Carnegie Mellon University, United States
* Walt Scacchi, University of California, Irvine, United States

## Organizing Committee

General Chairs:

* Filipe Saraiva (Universidade Federal do Pará, Brazil)
* Igor Scaliante Wiese (Universidade Tecnológica Federal do Paraná, Brazil)
