require 'fileutils'

papers = %[
  0034 0037
  0038 0040
  0042 0045
  0046 0049
  0050 0053
  0054 0057
  0058 0061
  0062 0065
  0066 0069
  0070 0073
  0074 0077
  0078 0081
  0082 0085
  0086 0089
  0090 0093
  0094 0097
  0098 0101
  0102 0105
  0106 0109
  0110 0113
]

papers.lines.map(&:strip).reject(&:empty?).map(&:split).each_with_index do |set, i|
  jpg_files = (set[0]..set[1]).map { |n| "scan/digitalizar#{n}.jpg" }
  output = "paper%02d.pdf" % (i+1)
  cmd = ['convert', *jpg_files, output]
  puts cmd.join(' ')
  system *cmd
end
