## Program Committee

* Adenauer Yamin, Universidade Federal de Pelotas/Universidade Católica de Pelotas, Brazil
* Alba Melo, Universidade de Brasília, Brazil
* Alexandre Ribeiro, Universidade de Caxias do Sul, Brazil
* Alfredo Goldman, Universidade de São Paulo, Brazil
* André Detsch, IBM/GoboLinux.org, Brazil
* André Leon Sampaio Gradvohl, Centro Nacional de Processamento de Alto Desempenho/Universidade São Francisco, Brazil
* Andrea Schwertner Charão, Universidade Federal de Santa Maria, Brazil
* Antonio Serra, Instituto Federal de Educação, Ciência e Tecnologia do Ceará, Brazil
* Autran Macedo, Universidade Federal de Uberlândia, Brazil
* Benhur de Oliveira Stein, Universidade Federal de Santa Maria, Brazil
* Celso Maciel da Costa, Pontifícia Universidade Católica do Paraná, Brazil
* Cristiano André da Costa, Universidade do Vale do Rio dos Sinos, Brazil
* Eduardo Todt, Universidade Federal do Paraná, Brazil
* Ewerton Longoni Madruga, Instituto Nacional de Metrologia, Qualidade e Tecnologia, Brazil
* Fábio Kon, Universidade de São Paulo, Brazil
* Fernando Santos Osório, Universidade de São Paulo, Brazil
* Frank Siqueira, Universidade Federal de Santa Catarina, Brazil
* Gaspare Bruno, Centro Universitário La Salle, Brazil
* Gerson Geraldo Homrich Cavalheiro, Universidade Federal de Pelotas, Brazil
* Giovanni Cordeiro Barroso, Universidade Federal do Ceará, Brazil
* Hisham Hashem Muhammad, GoboLinux.org, Brazil
* João Cesar Netto, Universidade Federal do Rio Grande do Sul, Brazil
* José Carlos Maldonado, Universidade de São Paulo, Brazil
* José Ramos Gonçalves, Universidade Federal do Ceará, Brazil
* Jussara Issa Musse, Universidade Federal do Rio Grande do Sul, Brazil
* Leonardo Lemes Fagundes, Universidade do Vale do Rio dos Sinos, Brazil
* Leu Cheuk Lung, Universidade Federal de Santa Catarina, Brazil
* Lisandro Zambenedetti Granville, Universidade Federal do Rio Grande do Sul, Brazil
* Lucas Ferrari de Oliveira, Universidade Federal de Pelotas, Brazil
* Luciano Paschoal Gaspary, Universidade Federal do Rio Grande do Sul, Brazil
* Luciano Porto Barreto, Universidade Federal da Bahia, Brazil
* Marcelo Finger, Universidade de São Paulo, Brazil
* Márcio Eduardo Delamaro, Universidade de São Paulo, Brazil
* Marcos Castilho, Universidade Federal do Paraná, Brazil
* Marilton Sanchotene de Aguiar, Universidade Católica de Pelotas, Brazil
* Mario Dantas, Universidade Federal de Santa Catarina, Brazil
* Noemi Rodriguez, Pontifícia Universidade Católica do Rio de Janeiro, Brazil
* Patrícia Kayser Vargas Mangan, Centro Universitário La Salle, Brazil
* Paulyne Matthews Jucá, Cesar/Universidade Federal de Pernambuco, Brazil
* Regina Borges de Araujo, Universidade Federal de São Carlos, Brazil
* Rejane Frozza, Universidade de Santa Cruz do Sul, Brazil
* Renata Galante, Universidade Federal do Rio Grande do Sul, Brazil
* Roberto Pinto Souto, Instituto Nacional de Pesquisas Espaciais, Brazil
* Rodrigo Araújo Real, Esteves & Salvador Ltda., Brazil
* Roland Teodorowitsch, Universidade Luterana do Brasil, Brazil
* Ronaldo Fernandes Ramos, Instituto Federal de Educação, Ciência e Tecnologia do Ceará, Brazil
* Silvio Cesar Cazella, Universidade do Vale do Rio dos Sinos, Brazil
* Simone André da Costa, Universidade Federal de Pelotas, Brazil

## Organizing Committee

General Chair

* Celso Maciel da Costa (Pontifícia Universidade Católica do Paraná, Brazil)

Program Committee Chair

* Andrea Schwertner Charão (Universidade Federal de Santa Maria, Brazil)

Organizing Committee

* Filipi Vianna (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)
* Maria Cristina Atz (Associação Software Livre, Brazil)
* Vinícius Vielmo Cogo (Universidade Federal de Santa Maria, Brazil)
* Vitor Conrado Faria Gomes (Universidade Federal de Santa Maria, Brazil)
