/* WSL - Free Software Workshop paper repository
 *
 * Copyright © 2012 Associação Software Livre.Org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

jQuery(function($) {

  function search() {
    var query = $(this).find('input.search-query').val().toLowerCase();

    if ((query == '') || (query == query.match(/[ ]{1,}/))){
        if($(location).attr('href').match('/search/')){
            $('.paper').hide();
        } else {
            $('.paper').show();
        }
      return false;
    }

    var found = false;
    $('.paper').each(function() {
      if ($(this).text().toLowerCase().match(query)) {
        $(this).show();
        found = true;
      } else {
        $(this).hide();
      }
    });

    if (found) {
      $('.no-results-found').hide();
    } else {
      $('.no-results-found').show();
    }

    return false;
  }

  $('form.form-search').submit(search);

  $('form.form-search .reset').click(function() {
    $('.paper').show();
    $('input.search-query').val('');
  })

  $('form.form-search .clear').click(function() {
    $('.paper').hide();
    $('input.search-query').val('');
  })

});
