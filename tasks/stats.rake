namespace 'stats' do

  desc 'Outputs CSV data for papers by year'
  task :by_year do
    puts 'year;number_of_papers'
    Dir.glob('*/data.yaml').sort.each do |f|
      year = File.dirname(f)
      data = YAML.load_file(f)
      puts [year, data['papers'].size].join(';')
    end
  end

end
