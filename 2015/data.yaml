papers:
  - title: "Participação de Mulheres em Projetos de Software Livre: Caso da Comunidade Brasileira da Mozilla Foundation"
    abstract: "Estudos e estatísticas mostram diversos motivos pelos quais mulheres não participam ou desistem de projetos de software livre. Esses dados fornecem uma visão a respeito da desproporcionalidade entre homens e mulheres em comunidades de software livre. Essa pesquisa aborda o reconhecimento dos motivos que levam mulheres a desistirem de participar de um projeto de software livre, bem como descobrir as áreas de colaboração onde se concentram a maioria das mulheres e onde elas são ausentes."
    file: "participacao-de-mulheres-em-projetos-de-software-livre:-caso-da-comunidade-brasileira-da-mozilla-foundation-wsl-2015.pdf"
    code: 1
    authors:
      - name: Mariana Martins Vargas Prudêncio
        institution: UTFPR
      - name: Filipe Roseiro Côgo
        institution: UEM
      - name: Ana Paula Chaves Steinmacher
        institution: UTFPR
      - name: Igor Steinmacher
        institution: UTFPR
  - title: "Fostering Free/Libre Open Source Software community formation: guidelines for communities to support newcomers' onboarding"
    abstract: "Community-based Free/Libre Open Source Software (FLOSS) projects are usually self-organized and dynamic, receiving contributions from distributed volunteers. These communities’ survival, long-term success, and continuity demand a constant influx of newcomers. However, newcomers face many barriers when making their first contribution to an OSS project, leading in many cases to dropouts. Therefore, a major challenge for OSS projects is to provide ways to support newcomers during their first contribution. In this paper, our goal was to provide a set of guidelines to communities that want to support and foster the participation of newcomers. The guidelines were proposed based on the analysis of data collected from: semi-structured interviews with 36 developers from 14 different projects; 24 answers to an open questionnaire conducted with OSS developers; feedback from 9 graduate and undergraduate students after they tried to join OSS projects; and 20 primary studies gathered via a systematic literature review."
    file: "fostering-freelibre-open-source-software-community-formation:-guidelines-for-communities-to-support-newcomers-onboarding-wsl-2015.pdf"
    code: 2
    authors:
      - name: Igor Steinmacher
        institution: UTFPR
      - name: Marco Gerosa
        institution: USP
  - title: "Formando para Criar: Agência Livre Colaborativa de Notícias"
    abstract: "In this article we present the process of creating a collaborative news agency based the principles of collaborative activity. We present the work done with 60 youngsters and the results achieved so far. We discuss parallels between notions of a “new journalism” and the ethics of free and open software."
    file: "formando-para-criar-agencia-livre-colaborativa-de-noticias-wsl-2015.pdf"
    code: 3
    authors:
      - name: Sarah Costa Schmidt
        institution: UNICAMP
      - name: Tel Amiel
        institution: UNICAMP
      - name: Robson Sampaio
        institution: UNICAMP
      - name: Daniel Pires
        institution: UNICAMP
  - title: "Software Livre e o respeito à vocação do homem em ser mais: relatos de Angola"
    abstract: "Este artigo busca estabelecer um diálogo sobre como o movimento do Software Livre pode contribuir para promoção de um ambiente de desenvolvimento humano e social em uma região subdesenvolvida, estando em consonância com uma visão de educação como prática de liberdade. Para tal fim, são apresentados relatos de uma experiência de voluntariado vivida pelo autor em Angola, que teve como escopo principal a implementação de tecnologias livres e desenvolvimento de processos educacionais relacionados à construção da autonomia tecnológica e do protagonismo juvenil."
    file: "software-livre-e-o-respeito-a-vocacao-do-homem-em-ser-mais-relatos-de-angola-wsl-2015.pdf"
    code: 4
    authors:
      - name: Alexandre Garcia Aguado
        institution: IFSP
  - title: "Dominando para não ser dominado: Autonomia tecnológica com o Projeto Jovem Hacker"
    abstract: "Este artigo apresenta o projeto Jovem Hacker, uma iniciativa que busca criar um currículo focado no conceito de ``end user programming''. Neste artigo apresentamos a trajetória do projeto, os resultados da primeira implementação, bem como as lições aprendidas impactaram o desenvolvimento da segunda edição do projeto. O curso será ofertado para jovens em duas cidades diferentes. Buscamos, através dessas iterações, gerar aprendizados sobre como melhor oferecer uma experiência de curto prazo que possa empoderar jovens a melhor interagir com software e tecnologia, no sentido abrangente."
    file: "dominando-para-nao-ser-dominado-autonomia-tecnologica-com-o-projeto-jovem-hacker-wsl-2015.pdf"
    code: 5
    authors:
      - name: Tel Amiel
        institution: UNICAMP
      - name: Gabriel de Souza Fedel
        institution: Coletivo Revoada
      - name: Flávia Linhalis Arantes
        institution: UNICAMP
      - name: Alexandre Garcia Aguado
        institution: IFSP
  - title: "Cuba, on a way to migrating to free software"
    abstract: "Since many years ago Cuba is passively immersed in a total and mass migration to Free software . This is a task that is performed and accomplished in various institutions of the country, and mainly driven by a discreet but active community of users who love Free Technologies. The process is slow, but it's reaping its benefits despite the different situations that stand in the way. To support this process, the Center for Free Software (CESOL) at the University of Information Sciences (UCI for its Spanish acronyms), has the social object of ushering Free Software migration into the country. In this paper a study of the needs of migrating to Free Software is conducted, which highlights the results achieved so far in the Cuban society."
    file: "cuba-on-a-way-to-migrating-to-free-software-wsl-2015.pdf"
    code: 6
    authors:
      - name: Yeicy Durán Quintana
        institution: UCI
      - name: Marielis González Muño
        institution: UCI
      - name: Mairim Delgado Muñiz
        institution: UCI
      - name: Marlies Quiala Torres
        institution: UCI
  - title: "Automato livre: semiótica e tecnologia adaptativa"
    abstract: "A partir de estudos de comunicação em chats de suporte de software livre, este trabalho discute a tecnologia adaptativa como opção para a automação livre, com foco no gerenciamento automático de diálogos. Discute-se também a proposta semiótica francesa como opção para análise da comunicação tendo em vista um refinamento no tratamento de dados verbais pelo gerenciador de diálogos."
    file: "automato-livre-semiotica-e-tecnologia-adaptativa-wsl-2015.pdf"
    code: 7
    authors:
      - name: Ana Cristina Fricke Matte
        institution: UFMG
  - title: "Um Estudo sobre Sustentabilidade de Projetos de Software Livre"
    abstract: "A sustentabilidade de projetos de software livre deve ser tratada de maneira ampla, sem pensar apenas no recurso financeiro em si, mas em toda a dinâmica envolvida no ciclo de vida de um projeto sustentável. Entende-se aqui por sustentável, o modelo de gestão em que é possível sustentar o projeto por meio de diversos recursos, sem que ele seja mantido financeiramente por uma única empresa ou instituição. Alguns aspectos particulares relacionados à sustentabilidade de projetos de software livre no contexto brasileiro também são discutidos neste artigo. A maioria dos projetos de software livre genuinamente brasileiros surgiram como resultados de pesquisas em universidades ou demandados por instituições do governo. A continuidade desses projetos depende grandemente da continuidade das pesquisas ou de financiamento do governo. O objetivo do estudo apresentado neste artigo é contribuir com a sustentabilidade de projetos de software livre, em geral, mas principalmente com casos de países como o Brasil, onde os projetos precisam sobreviver com voluntários ou poucos recursos financeiros. Este artigo apresenta uma revisão da literatura na área de sustentabilidade de projetos de software livre considerando não apenas os recursos financeiros, mas também o crescimento da comunidade, o gerenciamento do código-fonte e das ferramentas de comunicação."
    file: "um-estudo-sobre-sustentabilidade-de-projetos-de-software-livre-wsl-2015.pdf"
    code: 8
    authors:
      - name: Flávia Arantes
        institution: UNICAMP
  - title: "Proceso de Consultoría en Migración a Código Abierto. Aplicación práctica en la Empresa Constructora de Obras de Arquitectura No 24"
    abstract: "Previo a la ejecución de un proceso de migración a Software Libre y Código Abierto es necesario conocer el estado de la institución en cuanto a tecnologías, personas y procesos. Para ello el Grupo Técnico Nacional ofrece a los Organismos de la Administración Central del Estado Cubano el servicio de Consultoría en Migración a Código Abierto, el que se rige por lo definido en la Guía Cubana de Migración a Software Libre. Este servicio persigue el objetivo de proponer una hoja de ruta que permita migrar satisfactoriamente la empresa hacia tecnologías libres. Se exponen las principales actividades y resultados obtenidos como parte del Servicio de Consultoría realizado en la Empresa Constructora de Obras de Arquitectura No 24."
    file: "proceso-de-consultoria-en-migracion-a-codigo-abierto-aplicacion-practica-en-la-empresa-constructora-de-obras-de-arquitectura-no-24-wsl-2015.pdf"
    code: 9
    authors:
      - name: Yurenia Hernández
        institution: UCI
      - name: Eduardo Cuesta
        institution: UCI
      - name: Abel García
        institution: UCI
  - title: "An Open Access Repository for the Workshop de Software Livre (WSL)"
    abstract: "This paper describes the design and implementation of an open access, free of charge repository for WSL. This repository is available at wsl.softwarelivre.org and contains all the papers from all the editions of WSL since its inception in the year of 2000. The source code of the repository is licensed under the GPLv3, and the papers published under the Creative Commons Attribution-NoDerivs 3.0 Unported (CC BY-ND 3.0) license."
    file: "an-open-access-repositor-for-the-workshop-de-software-livre-WSL-wsl-2015.pdf"
    code: 10
    authors:
      - name: Filipe Saraiva
        institution: USP
      - name: Antonio Terceiro
        institution: COLIVRE/UnB
  - title: "Construindo um Scanner de Baixíssimo Custo"
    abstract: "A scanner is an indispensable tool for agencies and organizations with activities related to education or culture, but the cost, speed and flexibility can be an impediment to its effective use. We present here the results of tests and research that resulted in a working prototype of a similar esquipament, easy to build even for people with little skill with the use of tools and a very low price. This scanner has a great potential for accessibility as it allows converting images containing characters into editable text, which in turn permits its reading by people with low vision or conversion to synthesized speech."
    file: "construindo-um-scanner-de-baixissimo-custo-wsl-2015.pdf"
    code: 11
    authors:
      - name: Antonio Fernando Da Cunha Penteado
        institution: UNICAMP
      - name: Tel Amiel
        institution: UNICAMP
  - title: "O Software Livre enquanto Bandeira do Movimento de Mulheres na TI"
    abstract: "O presente trabalho faz parte de uma pesquisa maior, que visa entender as diferentes atuações de mulheres organizadas em grupo no sentido do empoderamento de mulheres na comunidade SL. Portanto, são apresentados alguns resultados referentes à observação e análise do movimento de mulheres na tecnologia, representado aqui pelo grupo /MNT – Mulheres na Tecnologia. Sendo assim, este artigo tem como objetivo conhecer quais são as perspectivas das mulheres sobre o papel da cultura livre neste processo de empoderamento. Ou seja, foca-se em entender o software livre enquanto uma bandeira do movimento de mulheres na TI no contexto nacional."
    file: "o-software-livre-enquanto-bandeira-do-movimento-de-mulheres-na-TI-wsl-2015.pdf"
    code: 12
    authors:
      - name: Mônica Paz
        institution: UFBA
  - title: "Proposta de uso da ética hacker na formação de estudantes do ensino fundamental"
    abstract: "This paper contains the development and the results of a research executed inside a public school from Passo Fundo, Rio Grande do Sul. The research aimed to investigate if children from elementary school do have characteristics of the hacker ethic in their learning process, and also, if, once stimulated, these characteristics would lead the child to become a hacker."
    file: "proposta-de-uso-da-etica-hacker-na-formacao-de-estudantes-do-ensino-fundamental-wsl-2015.pdf"
    code: 13
    authors:
      - name: Marcelo Araldi
        institution: IMED
      - name: Amilton Rodrigo de Quadros Martins
        institution: IMED
      - name: Adriano Canabarro Teixeira
        institution: UPF
  - title: "Notification System for Development Platforms of Continuous Integration inside the Cuban Distribution Pattern of GNU/Linux Nova"
    abstract: "With the development of the current research it was possible to implement a notification system, within the Platform for Development and Continuous Integration relevant to the Cuban distribution of GNU / Linux, to notify users in an efficient and scalable way about the events related to the different tools involved in the construction process. A research of protocols and of middleware for message passing was conducted, with a deeper and more updated perspective of the present situation. Aspects of the proposed solution are also exposed, and the verification of their scalability and efficiency has taken place. Its main result is the obtaining of a functional system, made up by a generator of notifications and a client application that is embedded in the notification bar of Nova distributions, informing users about the platform by means of a publication/subscription mechanism. Furthermore, an architecture that easily supports the integration of different communication channels has been conceived, thus ensuring scalability and efficiency."
    file: "notification-system-for-development-platforms-of-continuous-integration-inside-the-cuban-distribution-pattern-of-GNULinux-nova-wsl-2015.pdf"
    code: 14
    authors:
      - name: Jesús Rabelo Pérez
        institution: UCI
      - name: Marielis González Muño
        institution: UCI
      - name: Héctor Pérez Baranda
        institution: UCI
      - name: Mairim Delgado Muñiz
        institution: UCI
  - title: "Herramienta para la Migración y Administración de Servicios Telemáticos (HMAST)"
    abstract: "Los procesos de migración realizados por el Centro de Software Libre en las diferentes instituciones cubanas requieren el cambio de las plataformas privativas instaladas en los servidores para sistemas GNU/Linux. En la actualidad el administrador de los servicios telemáticos de la empresa o institución donde se realiza el proceso de migración administra los servidores, provocando en ocasiones que sea una tarea engorrosa y la existencia de pérdida de datos o no darse cuenta en algún cambio de los ficheros de configuración el cual haya afectado la ejecución de los servicios de estos servidores. La presente investigación tiene como objetivo desarrollar una herramienta sobre tecnologías libres, que de forma remota, realice el proceso de migración de los servidores y su administración. Para ello, se realiza un estudio de las principales herramientas existentes para la administración y migración de servicios telemáticos con el objetivo de obtener sus mejores funcionalidades y adicionarlas al producto final. También se documentan las tecnologías, herramientas y lenguajes de programación utilizados, todo guiado por la metodología de desarrollo SXP."
    file: "herramienta-para-la-migracion-y-administracion-de-servicios-telematicos-HMAST-wsl-2015.pdf"
    code: 15
    authors:
      - name: María González Carrera
        institution: UCI
      - name: Yurenia Hernández Blanco
        institution: UCI
      - name: Yadiel Pérez Villazón
        institution: UCI
      - name: Jailen García González
        institution: UCI
