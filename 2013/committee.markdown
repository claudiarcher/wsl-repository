## Program Committee

* Ana Cristina Matte, Universidade Federal de Minas Gerais, Brazil
* Andre Leme Fleury, Universidade de São Paulo, Brazil
* Anthony Wasserman, Carnegie Mellon University, USA
* Antonio Terceiro, Universidade Federal da Bahia, Brazil
* Célia Ralha, Universidade de Brasília, Brazil
* Christiana Freitas, Universidade de Brasília, Brazil
* Christopher Kelty, University of California, USA
* Daniel Batista, Universidade de São Paulo, Brazil
* Fabio Kon, Universidade de São Paulo, Brazil
* Gabriella Coleman, McGill University, Canada
* Islena Garcia, Universidade Estadual de Campinas, Brazil
* Jelena Karanovic, New York University, USA
* Lilly Nguyen, University of California, USA
* Masayuki Hatta, Surugadai University, Japan
* Tim Davies, University of Southampton, UK
* Stefan Koch, Bogazici University, Turkey

## Organizing Committee

General Chair

* Carlos D. Santos (Universidade de Brasília, Brazil)

Program Committee Chair

* Luis Felipe R. Murillo (University of California, USA) 

Local Arrangements and Publication Chair

* Paulo Meirelles (Universidade de São Paulo, Brazil)

Publicity Co-Chairs

* Imed Hammouda (Tampere University of Technology, Finland)
* Gregorio Robles (Universidad Rey Juan Carlos, Spain)
* Rafael Evangelista (Universidade Estadual de Campinas, Brazil)
* Veronica Xhardez (Flacso/CONICET - SoLAr, Argentina)
* Yuri Takhteyev  (University of Toronto, Canada)
